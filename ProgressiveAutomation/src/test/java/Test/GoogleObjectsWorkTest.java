package Test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import Page.GoogleObjectsWork;


public class GoogleObjectsWorkTest {
	static WebDriver driver;

	public static void main(String[] args) {
		invokeBrowser();
		performActions();
		tearDownTest();
	}
	//1: Invoke the browser
			public static void invokeBrowser() {
			//Setting the system path to utilize the chrome driver
			System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
					
			//Creating Webdriver object to drive my browser
			driver = new ChromeDriver();
			//Invoking the web browser
			driver.get("http://www.google.com");
					
			//Maximize the browser
			driver.manage().window().maximize();
			}
			
		//2: Perform Actions
			public static void performActions() {
				GoogleObjectsWork obj = new GoogleObjectsWork(driver);
				obj.GoogleSearch("TestNg");
			}
		//3: End Test case
			public static void tearDownTest() {
				driver.close();
				driver.quit();
			}
			

}
