package Test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import Page.ProgressiveAutoProject;

public class ProgressiveAutoProjectTest {
	static WebDriver driver;

	public static void main(String[] args) {
		invokeBrowser();
		completeForm();
	}
	//Invoke the browser
	public static void invokeBrowser() {
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://progressive.com");
		driver.manage().window().maximize();
	}
	//Complete the form
	public static void completeForm() {
		ProgressiveAutoProject obj = new ProgressiveAutoProject(driver);
		obj.clickAuto();
		obj.enterZip("84403");
		obj.clickGetaCode();
		obj.enterFirstName("Michael");
		obj.enterMidInitial("I");
		obj.enterLastName("Lopez");
		obj.selectSuffix("I");
		obj.enterDateofBirth("02/01/2000");
		obj.enterAddress("1220 Oram Circle");
		obj.enterCity("Logan");
		obj.enterUnit("null");
		//obj.enterZipcode("10101");
		obj.clickPOBox();
		
		
	}

}
