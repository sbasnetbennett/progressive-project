package TestNg;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.reporters.jq.Main;

import Page.ProgressiveAutoProject;

public class ProgressiveTestNg {
	static WebDriver driver;
	
	@BeforeTest
	public void setUpTest() {
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver();
	}
	public static void invokeBrowser() {
		driver.get("http://progressive.com");
		driver.manage().window().maximize();
		driver.navigate().refresh();
		System.out.println(driver.getTitle() + "........ was launched");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	@Test
		
	public static void performAction() {
		invokeBrowser();
		ProgressiveAutoProject obj = new ProgressiveAutoProject(driver);
		obj.clickAuto();
		obj.enterZip("84403");
		obj.clickGetaCode();
		obj.enterFirstName("Michael");
		obj.enterMidInitial("I");
		obj.enterLastName("Lopez");
		obj.selectSuffix("I");
		obj.enterDateofBirth("02/01/2000");
		obj.enterAddress("1220 Oram Circle");
		obj.enterCity("Logan");
		obj.enterUnit("null");
		obj.clickPOBox();
		
		performAction();
		
	}
	@AfterTest
	public static void termination() {
		/*
		 * driver.close(); driver.quit();
		 */
	}
}
