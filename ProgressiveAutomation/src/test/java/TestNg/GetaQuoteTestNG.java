package TestNg;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Framework.Frameworkclass;
import Page.AddPropertyPage;
import Page.ContinuePage;
import Page.DriversPage;
import Page.FormPage;
import Page.GetaCodePage;
import Page.InsuranceHistoryPage;
import Page.SnapShotPage;
import Page.HomePage;
import Page.SpousePage;
import Page.SpouseVerificationPage;
import Page.VehicleListPage;
import Page.VehiclesPage;
import Page.VerificationPage;
import screenshot.ScreenshotUtility;

public class GetaQuoteTestNG {
	static WebDriver driver;

	@BeforeTest
	public void setUpTest() {
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		// Invoke the Browser
		driver = new ChromeDriver();
		driver.get("http://www.progressive.com");
		driver.manage().window().maximize();
		driver.navigate().refresh();
		ScreenshotUtility.captureScreenShot(driver, "Progressive Home Page");
		System.out.println(driver.getTitle() + "......was launched");
	}

	@Test(priority = 0)
	public void performAction() throws InterruptedException {
		// First page
		HomePage homePage = new HomePage(driver);
		Thread.sleep(3000);
		// homePage.clickxBox();
		homePage.clickonAuto();
	}

	@Test(priority = 1)
	public void enteringZipCode() throws InterruptedException {
		// second page
		GetaCodePage getCode = new GetaCodePage(driver);
		Thread.sleep(3000);
		getCode.getACode("84403");
		ScreenshotUtility.captureScreenShot(driver, "ZipCodePage screenshot");
	}

	// Third page
	@Test(priority = 2)
	public void fillForm() throws InterruptedException {
		FormPage formPage = new FormPage(driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		formPage.enterName("Sam");
		formPage.enterInitial("I");
		formPage.enterLastName("Smith");
		formPage.selectSuffix("II");
		formPage.enterDateofBirth("01/01/19910");
		Thread.sleep(3000);

		// Using Tab
		Actions act = new Actions(driver);
		act.sendKeys(Keys.TAB).build().perform();
		act.sendKeys(Keys.RETURN).build().perform();

		Thread.sleep(2000);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		formPage.enterAddress("1210 City");
		Thread.sleep(3000);
		formPage.enterCity("NY");
		formPage.clickpoBox();
		formPage.clickForQuote();
		ScreenshotUtility.captureScreenShot(driver, "Personal detail screenshot");
	}

	@Test(priority = 3)
	public void vehicleDetail() throws InterruptedException {
		VehicleListPage vehicleList = new VehicleListPage(driver);
		Thread.sleep(5000);
		vehicleList.selectVehicleYear("2018");
		Thread.sleep(3000);
		vehicleList.selectVehicleMake("Ford");
		Thread.sleep(3000);
		vehicleList.selectModel("Mustang");
		Thread.sleep(4000);
		vehicleList.selectBodyType("2DR 4CYL");
		Thread.sleep(4000);
		vehicleList.selectPrimaryUse("Personal (to/from work or school, errands, pleasure)");
		Thread.sleep(3000);
		vehicleList.clickRideSharing();
		Thread.sleep(3000);
		vehicleList.selectOwnLease("Own");
		Thread.sleep(3000);
		vehicleList.selectLengthofOwnership("1 month - 1 year");
		Thread.sleep(3000);
		vehicleList.selectAutomaticEmergenccyBreak();
		vehicleList.clickDone();
		Thread.sleep(3000);
		vehicleList.clickContinue();
		ScreenshotUtility.captureScreenShot(driver, "Vehicle detail screenshot");
	}

	/*
	 * public void vehicleDetail() throws InterruptedException { //Fourth Page
	 * VehiclesPage vehiclePage = new VehiclesPage(driver); Thread.sleep(5000);
	 * driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	 * vehiclePage.selectYear(); Thread.sleep(4000); vehiclePage.selectMake();
	 * Thread.sleep(4000); vehiclePage.selectModel(); Thread.sleep(4000);
	 * vehiclePage.selectBodyType("2DR 4CYL"); Thread.sleep(4000); vehiclePage.
	 * selectPrimaryUse("Personal (to/from work or school, errands, pleasure)");
	 * Thread.sleep(3000); vehiclePage.clickRideSharing(); Thread.sleep(3000);
	 * vehiclePage.selectOwnLease("Own"); Thread.sleep(3000);
	 * vehiclePage.selectLengthofOwnership("1 month - 1 year"); Thread.sleep(3000);
	 * vehiclePage.selectAutomaticEmergenccyBreak(); vehiclePage.clickDone();
	 * Thread.sleep(3000); vehiclePage.clickContinue(); }
	 */
	@Test(priority = 4)
	public void DriversDetail() throws InterruptedException {
		DriversPage driversPage = new DriversPage(driver);
		Thread.sleep(3000);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driversPage.clickGender();
		Thread.sleep(3000);
		driversPage.selectMaritalStatus("Married");
		Thread.sleep(3000);
		driversPage.selectHighestEducation("Graduate work or graduate degree");
		Thread.sleep(3000);
		driversPage.selectEmploymentStatus("Not working/Other");
		Thread.sleep(3000);
		driversPage.enterSocialSecurityNumber("123-12-1234");
		Thread.sleep(3000);
		driversPage.selectPrimaryResidence("Own home");
		Thread.sleep(3000);
		driversPage.selectPriorAddress("No");
		Thread.sleep(3000);
		driversPage.selectLicenseStatus("Permit");
		Thread.sleep(3000);
		driversPage.selectDriverYearsLicense("Less than 1 year");
		Thread.sleep(3000);
		driversPage.clickAccidentOrClaim();
		Thread.sleep(3000);
		driversPage.clickTicketOrViolation();
		Thread.sleep(3000);
		driversPage.clickForContinue();
		ScreenshotUtility.captureScreenShot(driver, "Driver detail screenshot");

	}

	@Test(priority = 5)
	public void spouseDetails() throws InterruptedException {
		SpousePage spousePage = new SpousePage(driver);
		Thread.sleep(5000);
		spousePage.enterFirstName("Ben");
		Thread.sleep(3000);
		spousePage.selectSuffix("I");
		Thread.sleep(3000);
		spousePage.clickGender();
		Thread.sleep(3000);
		spousePage.enterDateofBirth("01/02/1985");
		Thread.sleep(3000);
		spousePage.clickDriverIncluded();
		Thread.sleep(3000);
		spousePage.selectLevelofEducation("Completed some college");
		Thread.sleep(3000);
		spousePage.selectEmploymentStatus("Not working/Other");
		Thread.sleep(3000);
		spousePage.selectLicenseStatus("Permit");
		Thread.sleep(3000);
		spousePage.selectYearLicensed("3 years or more");
		Thread.sleep(3000);
		spousePage.clickAccidentsOrClaims();
		Thread.sleep(3000);
		spousePage.clickTicketOrViolations();
		Thread.sleep(3000);
		spousePage.clickForContinue();
		ScreenshotUtility.captureScreenShot(driver, "Spouse detail screenshot");
	}

	@Test(priority = 6)
	public void clickContinue() throws InterruptedException {
		ContinuePage continuePage = new ContinuePage(driver);
		Thread.sleep(5000);
		continuePage.clickToContinue();
	}

	@Test(priority = 7)
	public void clickToContinue() throws InterruptedException {
		VerificationPage verification = new VerificationPage(driver);
		Thread.sleep(5000);
		verification.clickToContinue();
		ScreenshotUtility.captureScreenShot(driver, "Verification Page screenshot");
	}

	@Test(priority = 8)
	public void clickForContinue() throws InterruptedException {
		SpouseVerificationPage verificationPage = new SpouseVerificationPage(driver);
		Thread.sleep(5000);
		verificationPage.enterSocialSecurity("123-77-1255");
		verificationPage.clickToContinue();
		ScreenshotUtility.captureScreenShot(driver, "Spouse verification Page screenshot");
	}

	@Test(priority = 9)
	public void InsuranceHistory() throws InterruptedException {
		InsuranceHistoryPage historyPage = new InsuranceHistoryPage(driver);
		Thread.sleep(5000);
		historyPage.clickspouseInsurance();
		Thread.sleep(3000);
		historyPage.clickinsuranceLastMonth();
		Thread.sleep(3000);
		historyPage.clickotherPolicies();
		Thread.sleep(3000);
		historyPage.enterPrimaryEmailAddress("land3432@hotmail.com");
		Thread.sleep(3000);
		historyPage.clicHouseholdAlcoholConsumption();
		Thread.sleep(3000);
		historyPage.clickNonDrinkerDiscount();
		Thread.sleep(3000);
		historyPage.selectTotalResidents("2");
		Thread.sleep(3000);
		historyPage.selectCurrentResidenceLength("More than 1 year");
		Thread.sleep(3000);
		historyPage.clickContinue();
		ScreenshotUtility.captureScreenShot(driver, "Insurance history screenshot");
	}

	@Test(priority = 10)
	public void SnapShot() throws InterruptedException {
		SnapShotPage snapshotPage = new SnapShotPage(driver);
		Thread.sleep(5000);
		snapshotPage.clickForSnapshotEnrollment();
		Thread.sleep(3000);
		snapshotPage.clickToContinue();
		ScreenshotUtility.captureScreenShot(driver, "Snapshot screenshot");
	}

	@Test(priority = 11)
	public void AddProperty() throws InterruptedException {
		AddPropertyPage addProperty = new AddPropertyPage(driver);
		Thread.sleep(5000);
		addProperty.clickToContinue();
		ScreenshotUtility.captureScreenShot(driver, "Add property screenshot");
	}
	@Test (priority= 12)
	public static void validQuote() {
		if(driver.findElement(By.xpath("(//button[contains(text(), 'Finish & Buy')])[1]")).getText().contains("Finish & Buy")) {
			ScreenshotUtility.captureScreenShot(driver, "Final quote screenshot");	
			System.out.println("Pass");
		}else {
			System.out.println("Fail");
		}
	}

	@AfterTest
	public void endTest() throws InterruptedException {
		Thread.sleep(8000);
		 //driver.close();
		 //driver.quit();
		//System.out.println("Terminating the Browser");
	}

}
