package TestNg;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Page.GoogleObjectsWork;

public class GooglePageTesntNg {
	//Class level variable
		static WebDriver driver;
		@BeforeTest
		public void setUpTest() {
			System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
			driver = new ChromeDriver();
		}
		public static void invokeBrowser() {
			driver.navigate().to("http://google.com");
			driver.navigate().refresh();
			
			System.out.println(driver.getTitle() + "........ was launched");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		@Test(priority = 0)
		public static void performActions() {
			// Navigate to google
			invokeBrowser();
			
			GoogleObjectsWork obj = new GoogleObjectsWork(driver);
			obj.GoogleSearch("TestNg");
			
		}
		@AfterTest
		public static void termination() {
			driver.close();
			driver.quit();
			
		}

}
