package Page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class InsuranceHistoryPage {
	WebDriver driver;
	
public InsuranceHistoryPage(WebDriver driver) {
	this.driver = driver;
	PageFactory.initElements(driver, this);
}

@FindBy(how = How.XPATH, using = "//input[@id='FinalDetailsEdit_embedded_questions_list_InsuranceToday_N']")
	public WebElement spouseInsurance;

@FindBy(how = How.ID, using = "FinalDetailsEdit_embedded_questions_list_InsuranceLastMonth_N")
public WebElement insuranceLastMonth;

@FindBy(how = How.ID, using = "FinalDetailsEdit_embedded_questions_list_OtherPolicies_N")
public WebElement otherPolicies;

@FindBy(how = How.ID, using = "FinalDetailsEdit_embedded_questions_list_PrimaryEmailAddress")
public WebElement primaryEmailAdd;

@FindBy(how = How.ID, using = "FinalDetailsEdit_embedded_questions_list_HouseholdAlcoholConsumption_N")
public WebElement householdAlcoholConsumption;

@FindBy(how = How.ID, using = "FinalDetailsEdit_embedded_questions_list_NonDrinkerDiscount_Y")
public WebElement nonDrinkerDiscount;

@FindBy(how = How.ID, using = "FinalDetailsEdit_embedded_questions_list_TotalResidents")
public WebElement totalResidents;

@FindBy(how = How.ID, using = "FinalDetailsEdit_embedded_questions_list_CurrentResidence")
public WebElement currentResidenceLength;

@FindBy(how = How.XPATH, using = "//button[contains(text(), 'Continue')]")
WebElement Continue;

public void clickspouseInsurance() {
	spouseInsurance.click();
}
public void clickinsuranceLastMonth() {
	insuranceLastMonth.click();
}
public void clickotherPolicies() {
	otherPolicies.click();
}
public void enterPrimaryEmailAddress(String emailAddress) {
	primaryEmailAdd.sendKeys(emailAddress);
}
public void clicHouseholdAlcoholConsumption() {
	householdAlcoholConsumption.click();
}
public void clickNonDrinkerDiscount() {
	nonDrinkerDiscount.click();
}
public void selectTotalResidents(String ResidentsNumber) {
	Select chooseTotalResidents = new Select(totalResidents);
	chooseTotalResidents.selectByVisibleText(ResidentsNumber);
}
public void selectCurrentResidenceLength(String lengthOfResidence) {
	Select chooseCurrentResidence = new Select(currentResidenceLength);
	chooseCurrentResidence.selectByVisibleText(lengthOfResidence);
}
public void clickContinue() {
	Continue.click();
}
}
