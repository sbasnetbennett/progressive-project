package Page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import Framework.Frameworkclass;

public class HomePage {
	WebDriver driver = null;
	
	public HomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//p[@class='txt']")
	public WebElement Auto;
	@FindBy(how = How.XPATH, using = "//a[@id='acsFocusFirst']")
	public WebElement popup;
	
	public void clickonAuto () {
		Auto.click();
	}
	
		public void clickxBox() {
			popup.click();
		}
	

}
