package Page;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class VehicleListPage {
	WebDriver driver = null;
	
	public VehicleListPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void selectVehicleYear(String year) {
		WebElement yearWebelement = driver.findElement(By.xpath("//*[@name='VehiclesNew_embedded_questions_list_Year']"));
		List<WebElement> YearList = yearWebelement.findElements(By.tagName("li"));
		for(WebElement selectYear : YearList) {
			if(selectYear.getText().equalsIgnoreCase(year)) {
				selectYear.click();
				break;
			}
		}
	}
	public void selectVehicleMake(String Make) {
		WebElement makeWebelement = driver.findElement(By.name("VehiclesNew_embedded_questions_list_Make"));
		List<WebElement> makeList = makeWebelement.findElements(By.tagName("li"));
		for (WebElement selectMake : makeList) {
			if(selectMake.getText().equalsIgnoreCase(Make)) {
				selectMake.click();
			}
		}
	}
	public void selectModel(String Model) throws InterruptedException {
		WebElement modelWebelement = driver.findElement(By.name("VehiclesNew_embedded_questions_list_Model"));
		List<WebElement> makeList = modelWebelement.findElements(By.tagName("li"));
		for (WebElement selectModel : makeList) {
		if(selectModel.getText().equalsIgnoreCase(Model)){
			selectModel.click();
			Thread.sleep(2000);
			break;
		}
		}
	}
	@FindBy(how = How.XPATH, using = "//select[@id='VehiclesNew_embedded_questions_list_BodyStyle']")
	public WebElement bodyType;
	
	@FindBy(how = How.XPATH, using = "//select[@id='VehiclesNew_embedded_questions_list_VehicleUse']")
	public WebElement primaryUse;
	
	@FindBy(how = How.XPATH, using = "//input[@id='VehiclesNew_embedded_questions_list_VehicleTransportNetworkCompanyIndicator']")
	public WebElement clickforRidesharing;
	
	//@FindBy(how = How.XPATH, using = "//input[@id='VehiclesNew_embedded_questions_list_GaragingZip']")
	//public WebElement zipLocation;
	
	@FindBy(how = How.XPATH, using = "//select[@id='VehiclesNew_embedded_questions_list_OwnOrLease']")
	public WebElement ownOrLeaseOrFinance;
	
	@FindBy(how = How.XPATH, using = "//select[@id='VehiclesNew_embedded_questions_list_LengthOfOwnership']")
	public WebElement lengthofOwnership;
	
	@FindBy(how = How.XPATH, using = "//input[@id='VehiclesNew_embedded_questions_list_AutomaticEmergencyBraking_Y']")
	public WebElement automaticEmergencyBreak;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Done')]")
	public WebElement clickdone;
	
	@FindBy(how = How.XPATH, using ="//button[contains(text(),'Continue')]")
	public WebElement clickcontinue;
	
	public void selectBodyType(String BodyType) {
		Select chooseBodytype = new Select(bodyType);
		chooseBodytype.selectByVisibleText(BodyType);
	}
	public void selectPrimaryUse(String PrimaryUse) {
		Select chooseprimaryUse = new Select(primaryUse);
		chooseprimaryUse.selectByVisibleText(PrimaryUse);
	}
	public void clickRideSharing() {
		clickforRidesharing.click();
	}
	public void selectOwnLease(String ownlease) {
		Select chooseOwnLease = new Select(ownOrLeaseOrFinance);
		chooseOwnLease.selectByVisibleText(ownlease);
	}
	public void selectLengthofOwnership(String length) {
		Select chooseLengthofOwnership = new Select(lengthofOwnership);
		chooseLengthofOwnership.selectByVisibleText(length);
	}
	public void selectAutomaticEmergenccyBreak() {
		automaticEmergencyBreak.click();
	}
	public void clickDone() {
		clickdone.click();
	}
	public void clickContinue() {
		clickcontinue.click();
	}
}
