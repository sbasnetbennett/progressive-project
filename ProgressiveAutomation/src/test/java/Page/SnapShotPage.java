package Page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class SnapShotPage {
	WebDriver driver;
	
public SnapShotPage(WebDriver driver) {
	this.driver = driver;
	PageFactory.initElements(driver, this);
}
@FindBy(how = How.ID, using = "SnapshotEnrollmentV2Edit_embedded_questions_list_SnapshotEnrollment.0_Y")
public WebElement snapshotEnrollment;

@FindBy(how = How.XPATH, using = "//button[contains(text(), 'Continue')]") 
WebElement Continue;

public void clickForSnapshotEnrollment() {
	snapshotEnrollment.click();
}
public void clickToContinue() {
	Continue.click();
}
}
