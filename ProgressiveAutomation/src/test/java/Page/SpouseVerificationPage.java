package Page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class SpouseVerificationPage {
	WebDriver driver;
	
	public SpouseVerificationPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using ="//input[@id='DriversEditNoCreditHitSpouseDetails_embedded_questions_list_SocialSecurityNumber']")
	public WebElement socialSecurity;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(), 'Continue')]")
	WebElement Continue;
	
	public void enterSocialSecurity(String socialSecurityNumber) {
		socialSecurity.sendKeys(socialSecurityNumber);
	}
	public void clickToContinue() {
		Continue.click();
	}
}
