package Page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class VehiclesPage {
	WebDriver driver;

	public VehiclesPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.XPATH, using = "//li[contains(text(),'2018')]")
	public WebElement vehicleYear;
	
	@FindBy(how = How.XPATH, using = "//li[contains(text(),'Ford')]")
	public WebElement vehicleMake;
	
	@FindBy(how = How.XPATH, using = "//li[contains(. , 'Mustang')]")
	public WebElement vehicleModel;
	
	@FindBy(how = How.XPATH, using = "//select[@id='VehiclesNew_embedded_questions_list_BodyStyle']")
	public WebElement bodyType;
	
	@FindBy(how = How.XPATH, using = "//select[@id='VehiclesNew_embedded_questions_list_VehicleUse']")
	public WebElement primaryUse;
	
	@FindBy(how = How.XPATH, using = "//input[@id='VehiclesNew_embedded_questions_list_VehicleTransportNetworkCompanyIndicator']")
	public WebElement clickforRidesharing;
	
	//@FindBy(how = How.XPATH, using = "//input[@id='VehiclesNew_embedded_questions_list_GaragingZip']")
	//public WebElement zipLocation;
	
	@FindBy(how = How.XPATH, using = "//select[@id='VehiclesNew_embedded_questions_list_OwnOrLease']")
	public WebElement ownOrLeaseOrFinance;
	
	@FindBy(how = How.XPATH, using = "//select[@id='VehiclesNew_embedded_questions_list_LengthOfOwnership']")
	public WebElement lengthofOwnership;
	
	@FindBy(how = How.XPATH, using = "//input[@id='VehiclesNew_embedded_questions_list_AutomaticEmergencyBraking_Y']")
	public WebElement automaticEmergencyBreak;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Done')]")
	public WebElement clickdone;
	
	@FindBy(how = How.XPATH, using ="//button[contains(text(),'Continue')]")
	public WebElement clickcontinue;
	
	public void selectYear() {
		vehicleYear.click();
	}
	public void selectMake() {
		vehicleMake.click();
	}
	public void selectModel() {
		vehicleModel.click();
	}
	public void selectBodyType(String BodyType) {
		Select chooseBodytype = new Select(bodyType);
		chooseBodytype.selectByVisibleText(BodyType);
	}
	public void selectPrimaryUse(String PrimaryUse) {
		Select chooseprimaryUse = new Select(primaryUse);
		chooseprimaryUse.selectByVisibleText(PrimaryUse);
	}
	public void clickRideSharing() {
		clickforRidesharing.click();
	}
	public void selectOwnLease(String ownlease) {
		Select chooseOwnLease = new Select(ownOrLeaseOrFinance);
		chooseOwnLease.selectByVisibleText(ownlease);
	}
	public void selectLengthofOwnership(String length) {
		Select chooseLengthofOwnership = new Select(lengthofOwnership);
		chooseLengthofOwnership.selectByVisibleText(length);
	}
	public void selectAutomaticEmergenccyBreak() {
		automaticEmergencyBreak.click();
	}
	public void clickDone() {
		clickdone.click();
	}
	public void clickContinue() {
		clickcontinue.click();
	}

	
	
	

}
