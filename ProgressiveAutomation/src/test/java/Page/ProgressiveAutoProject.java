package Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class ProgressiveAutoProject {
	WebDriver driver;
	By clickAuto = By.xpath("//p[@class='txt']");
	By enterZip = By.xpath("//input[@id='zipCode_overlay']");
	By clickGetaCode = By.xpath("//input[@name='qsButton']");
	By enterFirstName = By.xpath("//input[@id='NameAndAddressEdit_embedded_questions_list_FirstName']");
	By enterMidInitial = By.xpath("//input[@id='NameAndAddressEdit_embedded_questions_list_MiddleInitial']");
	By enterLastName = By.xpath("//input[@id ='NameAndAddressEdit_embedded_questions_list_LastName']");
	By selectSuffix = By.xpath("//select[@id ='NameAndAddressEdit_embedded_questions_list_Suffix']");
	By enterDateofBirth = By.xpath("//input[@analytics-id='NameAndAddressEdit_DateOfBirth']");
	By enterAddress = By.xpath("//input[@analytics-id='NameAndAddressEdit_MailingAddress']");
	By enterUnit = By.xpath("//input[@id='NameAndAddressEdit_embedded_questions_list_ApartmentUnit']");
	By enterCity = By.xpath("//input[@id='NameAndAddressEdit_embedded_questions_list_City']");
	//By enterZipcode = By.xpath("//input[@id='NameAndAddressEdit_embedded_questions_list_ZipCode']");
	By clickPOBox = By.xpath("//input[@id='NameAndAddressEdit_embedded_questions_list_MailingZipType']");
	
	public ProgressiveAutoProject(WebDriver driver) {
		this.driver = driver;
	}
	public void clickAuto () {
		driver.findElement(clickAuto).click();
	}
	public void enterZip (String zipCode) {
		driver.findElement(enterZip).sendKeys(zipCode);
	}
	public void clickGetaCode () {
		driver.findElement(clickGetaCode).click();
	}
	public void enterFirstName (String firstName) {
		driver.findElement(enterFirstName).sendKeys(firstName);
	}
	public void enterMidInitial (String M) {
		driver.findElement(enterMidInitial).sendKeys(M);
	}
	public void enterLastName (String lastName) {
		driver.findElement(enterLastName).sendKeys(lastName);
	}
	public void selectSuffix (String suffix) {
		WebElement element = driver.findElement(selectSuffix);
		Select chooseSuffix = new Select(element);
		chooseSuffix.selectByVisibleText(suffix);
	}
	public void enterDateofBirth (String dateofBirth) {
		driver.findElement(enterDateofBirth).sendKeys(dateofBirth);
	}
	public void enterAddress (String address) {
		driver.findElement(enterAddress).sendKeys(address);
	}
	public void enterUnit (String unit) {
		driver.findElement(enterUnit).sendKeys(unit);
	}
	public void enterCity (String cityName) {
		driver.findElement(enterCity).sendKeys(cityName);
	}
	/*public void enterZipcode (String zipCode) {
		driver.findElement(enterZipcode).sendKeys(zipCode);
	}*/
	public void clickPOBox () {
		driver.findElement(clickPOBox).click();
	}

}
