package Page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class GetaCodePage {
	WebDriver driver = null;
	
	public GetaCodePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//input[@id='zipCode_overlay']")
	public WebElement zipCode;
	
	@FindBy(how = How.XPATH, using ="//input[@name='qsButton']")
	public WebElement clickGetaCode;
	
	public void getACode(String enterZip) {
		zipCode.sendKeys(enterZip);
		clickGetaCode.click();
	}
	

}
