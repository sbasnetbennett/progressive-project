package Page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class SpousePage {
	WebDriver driver;
	public SpousePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.ID, using = "DriversAddSpouseDetails_embedded_questions_list_FirstName")
	public WebElement firstName;
	
	@FindBy(how = How.ID, using = "DriversAddSpouseDetails_embedded_questions_list_LastName")
	public WebElement lastName;
	
	@FindBy(how = How.ID, using = "DriversAddSpouseDetails_embedded_questions_list_Suffix")
	public WebElement Suffix;
	
	@FindBy(how = How.ID, using = "DriversAddSpouseDetails_embedded_questions_list_Gender_M")
	public WebElement Gender;
	
	@FindBy(how = How.ID, using = "DriversAddSpouseDetails_embedded_questions_list_DateOfBirth")
	public WebElement dateOfBirth;
	
	@FindBy(how = How.ID, using = "DriversAddSpouseDetails_embedded_questions_list_DriverIncluded_Y")
	public WebElement driverIncluded;
	
	@FindBy(how = How.ID, using = "DriversAddSpouseDetails_embedded_questions_list_HighestLevelOfEducation")
	public WebElement highestLevelofEducation;
	
	@FindBy(how = How.ID, using = "DriversAddSpouseDetails_embedded_questions_list_EmploymentStatus")
	public WebElement employmentStatus;
	
	@FindBy(how = How.ID, using = "DriversAddSpouseDetails_embedded_questions_list_LicenseStatus")
	public WebElement licenseStatus;
	
	@FindBy(how = How.ID, using = "DriversAddSpouseDetails_embedded_questions_list_DriverYearsLicensed")
	public WebElement driverYearsLicensed; 
	
	@FindBy(how = How.ID, using = "DriversAddSpouseDetails_embedded_questions_list_HasAccidentsOrClaims_N")
	public WebElement accidentsOrClaims;
	
	@FindBy(how = How.ID, using = "DriversAddSpouseDetails_embedded_questions_list_HasTicketsOrViolations_N")
	public WebElement ticketOrViolations;
	
	@FindBy(how = How.XPATH, using = "//button[contains(text(),'Continue')]")
	public WebElement clickContinue;
	
	public void enterFirstName(String fName) {
		firstName.sendKeys(fName);
	}
	//public void enterLastName(String lName) {
		//lastName.sendKeys(lName);
	//}
	public void selectSuffix(String suffix) {
		Select chooseSuffix = new Select(Suffix);
		chooseSuffix.selectByVisibleText(suffix);
	}
	public void clickGender() {
		Gender.click();
	}
	public void enterDateofBirth(String DOB) {
		dateOfBirth.sendKeys(DOB);
	}
	public void clickDriverIncluded() {
		driverIncluded.click();
	}
	public void selectLevelofEducation(String Education) {
		Select chooseLevelOfEducation = new Select(highestLevelofEducation);
		chooseLevelOfEducation.selectByVisibleText(Education);
	}
	public void selectEmploymentStatus(String EmploymentStatus) {
		Select chooseEmploymentStatus = new Select(employmentStatus);
		chooseEmploymentStatus.selectByVisibleText(EmploymentStatus);
	}
	public void selectLicenseStatus(String Licensestatus) {
		Select chooseLicenseStatus = new Select(licenseStatus);
		chooseLicenseStatus.selectByVisibleText(Licensestatus);
	}
	public void selectYearLicensed(String yearLicensed) {
		Select chooseYearLicensed = new Select(driverYearsLicensed);
		chooseYearLicensed.selectByVisibleText(yearLicensed);
	}
	public void clickAccidentsOrClaims() {
		accidentsOrClaims.click();
	}
	public void clickTicketOrViolations() {
		ticketOrViolations.click();
	}
	public void clickForContinue() {
		clickContinue.click();
	}
	
}
